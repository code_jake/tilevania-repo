using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class playerMovement : MonoBehaviour
{
    [SerializeField]float playerSpeed = .2f;
    Rigidbody2D myRigidBody;
    Vector2 MoveInput; 
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
    Run();
    flipSprite();
    }

    void OnMove(InputValue value) 
    {
        MoveInput = value.Get<Vector2>();
        Debug.Log(MoveInput);
        
    }
    
    void Run() 
    {
        Vector2 playerVelocity = new Vector2 (MoveInput.x * playerSpeed,myRigidBody.velocity.y);
        myRigidBody.velocity = playerVelocity;
       
    }

    void flipSprite()
    {
        transform.localScale = new Vector2 (Mathf.Sign(myRigidBody.velocity.x),1f);
    }
}
